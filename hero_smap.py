import json
import re

# https://catlikecoding.com/unity/tutorials/hex-map/part-1/hexagonal-coordinates/cube-diagram.png

# https://pypi.org/project/hexutil/
# https://github.com/rosshamish/hexgrid/


def parse_smap_from_log_content(content):
    return json.loads(re.search(r'\s*var m = (\[.*\]);', content).group(1))


TILE_TYPE = {
        '?': '?',
        ' ': 'water',

        '!': 'mark_red',
        '&': 'mark_lightblue',
        '(': 'mark_blue',
        ')': 'mark_purple',
        '^': 'mark_green',
        '`': 'mark_orange',
        '~': 'mark_yellow',

        'a': 'arrow_w',
        'c': 'arrow_se',
        'd': 'arrow_e',
        'e': 'arrow_ne',
        'q': 'arrow_nw',
        'w': 'arrow_n',
        'x': 'arrow_s',
        'z': 'arrow_sw',

        '<': 'island_food',
        '>': 'island_help',
        'i': 'island_unknown',
        'm': 'island_lighthouse',
        'n': 'island_repair',
        'v': 'island_gp',

        '#': 'border',
        ',': 'reef',
        ';': 'ice',
        '@': 'vortex',
        'B': 'fish_moving',
        'I': 'island_explored',
        'b': 'fish',
        'p': 'port',

        '[': 'island_radius_11',
        ']': 'island_radius_none',
        'o': 'island_radius_9',
        't': 'island_radius_3',
        'u': 'island_radius_7',
        'y': 'island_radius_5',


        '1': 'ship_1',
        '2': 'ship_2',
        '3': 'ship_3',
        '4': 'ship_4',

        'j': 'fish_flag_in_hole?',
        'J': 'fish_devil?',
        'h': 'fish_frog?',
        'H': 'fish_skull?',

        'M': 'island_male',
        'F': 'water_female',
        'f': 'island_female',

        'g': 'water_treasure',
        'G': 'island_treasure',
        '$': 'island_border',
}


def parse_smap_tile(e):
    # returns (NW-SE, E-W, NE-SW, type)
    rv = []
    for i in range(4):
        n = 255 & e
        e = int((e - n) / 256)
        if n > 127:
            n -= 256
        if i == 3:
            try:
                n = TILE_TYPE[chr(n)]
            except KeyError:
                raise KeyError('Unknown type {} for tile {}'.format(chr(n), rv))
        rv.append(n)
    return rv


def print_smap(smap):
    smap = tuple(sorted(parse_smap_tile(x) for x in smap))

    counter = {}
    for i, row in enumerate(smap):
        print(i, row)
        if row[3] not in counter:
            counter[row[3]] = 0
        counter[row[3]] += 1

    for k, v in sorted(((v, k) for k, v in counter.items()), reverse=True):
        print(k, v)


print_smap(parse_smap_from_log_content(open('keug8sm.html').read()))
