from datetime import timedelta

import colorama

from hero_utils import now


def _get_avail(name, after, avail):
    if after:
        delta = timedelta(seconds=int(after))
        at = now(False) + delta
        return '{}:{}({})'.format(name, delta, at)
    elif avail:
        return name + ':{Style.BRIGHT}AVAIL{Style.NORMAL}'


class HeroPrint:
    def __init__(self, **kwargs):
        colorama.init()
        super().__init__(**kwargs)

    def print(self, *args, **kwargs):
        print(self.format(*args, **kwargs), colorama.Style.RESET_ALL)

    def format(self, *args, **kwargs):
        if 'locals' in kwargs:
            locals_ = kwargs.pop('locals')
            locals_.pop('self', None)
            kwargs.update(locals_)
        kwargs.update(Fore=colorama.Fore, Back=colorama.Back, Style=colorama.Style,
                      **self.info)
        return ' '.join([arg.format(**kwargs) for arg in args])

    @staticmethod
    def format_health_percent(percent):
        percent = percent or 0
        if percent <= 33:
            color = colorama.Back.RED
        elif percent <= 66:
            color = colorama.Back.YELLOW
        else:
            color = colorama.Back.GREEN
        return ('{color}{Style.BRIGHT}({percent:02.0f}%){Style.RESET_ALL}'
                .format(color=color, percent=percent, Style=colorama.Style))

    @staticmethod
    def format_damage(damage):
        if isinstance(damage, float):
            damage_str = '{:02.2f}'.format(abs(damage))
        elif isinstance(damage, int):
            damage_str = '{:02.0f}'.format(abs(damage))
        if damage < 0:
            return '[{}+{}{}]'.format(colorama.Fore.GREEN, damage_str, colorama.Fore.RESET)
        elif damage > 0:
            return '[{}-{}{}]'.format(colorama.Fore.RED, damage_str, colorama.Fore.RESET)
        else:
            return '[0]'

    def print_hero(self):
        info = self.info

        if info.hero.aura_name:
            delta = timedelta(seconds=info.hero.aura_time)
            aura = self.format(' {Fore.CYAN}AURA:{Fore.RESET}{hero.aura_name}:'
                               '{Style.BRIGHT}{delta}{Style.NORMAL}({at})',
                               delta=delta, at=now(False) + delta)
        else:
            aura = ''
        self.print(
            '"{hero.name}" {Fore.GREEN}LVL:{Fore.RESET}{hero.level}'
            '({Style.BRIGHT}{hero.exp_progress}%{Style.NORMAL})[{hero.alignment}] '
            '{Fore.RED}HP:{Fore.RESET}'
            '{Style.BRIGHT}{hero.health}{Style.NORMAL}/{hero.max_health}{health_percent_str} '
            '{Fore.BLUE}GP:{Style.BRIGHT}{hero.godpower}%{Style.RESET_ALL} '
            '{Fore.BLUE}ACC:{Fore.RESET}{Style.BRIGHT}{hero.accumulator:.0f}{Style.NORMAL}{aura}',
            health_percent_str=self.format_health_percent(info.health_percent), aura=aura,
        )

        if info.hero.quest and info.hero.quest.endswith('(эпик)'):
            quest_type = self.format('[{Style.BRIGHT}EPIC{Style.NORMAL}]')
        elif info.hero.quest and info.hero.quest.endswith('(мини)'):
            quest_type = self.format('[{Style.BRIGHT}MINI{Style.NORMAL}]')
        elif info.hero.quest and info.hero.quest.endswith('(пет)'):
            quest_type = self.format('[{Style.BRIGHT}PET{Style.NORMAL}]')
        elif info.hero.quest and info.hero.quest.endswith('(гильд)'):
            quest_type = self.format('[{Style.BRIGHT}GUILD{Style.NORMAL}]')
        elif info.hero.quest and info.hero.quest.endswith('(отменено)'):
            quest_type = self.format('[{Style.BRIGHT}CANCELED{Style.NORMAL}]')
        else:
            quest_type = ''

        if info.hero.get('q2'):
            # print(info.hero.q2)
            q2 = info.hero.q2[0]
            q2_progress = info.hero.q2[1]
        else:
            q2, q2_progress = None, None

        if 'wood_cnt' in info.hero:
            res_str = self.format('{Fore.GREEN}WD:{Fore.RESET}'
                                  '{Style.BRIGHT}{hero.wood_cnt:,}{Style.NORMAL}')
            if info.hero.get('ark_m') or info.hero.get('ark_f'):
                if info.hero.get('ark_m', 0) < 1000 or info.hero.get('ark_f', 0) < 1000:
                    res_str += self.format(
                        ' {Fore.BLUE}ARK:{Fore.RESET}'
                        '{Style.BRIGHT}{hero.ark_m},{hero.ark_f}{Style.NORMAL}')
                else:
                    res_str += self.format(
                        ' {Fore.BLUE}LAB:{Fore.RESET}'
                        '{Style.BRIGHT}{lab_m},{lab_f}{Style.NORMAL}')
            if info.hero.get('wrd'):
                res_str += self.format(
                    ' {Fore.YELLOW}WRD:{Fore.RESET}'
                    '{Style.BRIGHT}{wrd_cnt}{Style.NORMAL}')
        else:
            res_str = self.format('{Fore.YELLOW}BRICKS:{Fore.RESET}'
                                  '{Style.BRIGHT}{hero.bricks_cnt:,}{Style.NORMAL}')
        if info.has_pet:
            is_dead = info.pet.pet_is_dead and '(DEAD)' or ''
            pet = self.format('{pet.pet_level}{is_dead}', is_dead=is_dead)
        else:
            pet = 'NO'
        self.print(
            '{Fore.YELLOW}QST:{Fore.RESET}{hero.quests_completed}'
            '{quest_type}({Style.BRIGHT}{hero.quest_progress}%{Style.NORMAL}) '
            '{Fore.YELLOW}$:{Style.BRIGHT}{hero.gold:,}{Style.RESET_ALL} {res_str} '
            '{Fore.MAGENTA}INV:{Fore.RESET}{Style.BRIGHT}'
            '{hero.inventory_num}{Style.NORMAL}/{hero.inventory_max_num}'
            '[@:{inventory_active_cnt} !:{inventory_bold_cnt} '
            'H:{inventory_heal_cnt} :{inventory_common_cnt}] '
            '{Fore.MAGENTA}PET:{Fore.RESET}{pet_}'
            + (' {Fore.BLUE}Q2:{q2}{Fore.RESET}'
               '({Style.BRIGHT}{q2_progress}%{Style.RESET_ALL})' if q2 else '')
            + ((_get_avail('', info.q2t_after, False) or '') if q2 else ''),
            quest_type=quest_type, pet_=pet, res_str=res_str, q2=q2, q2_progress=q2_progress,
        )

    def print_activity(self):
        if self.info.hero.get('poi'):
            maybe_poi = ' o:' + ','.join(map(str, self.info.hero.poi))
        else:
            maybe_poi = ''

        if self.info.activity_type == 'TOWN':
            # assert not self.info.hero.distance
            self.print(
                '{Fore.BLUE}TOWN:{hero.town_name}{Style.RESET_ALL} |:{hero.distance}'
                '{Fore.BLUE}{maybe_poi}{Fore.RESET}',
                'news_from_field' in self.info and
                ('{Style.BRIGHT}{hero.s_progress}%{Style.RESET_ALL} - '
                 '{news_from_field.msg}') or '', maybe_poi=maybe_poi)

        if self.info.activity_type == 'MONSTER':
            self.print('{Fore.RED}MONSTER:{hero.monster_name}' +
                       (self.info.hero.get('monster_s') and '{Style.BRIGHT}[!]' or '') +
                       '{Style.RESET_ALL} '
                       '|:{hero.distance}[{near_town_short}]{Fore.BLUE}{maybe_poi}{Fore.RESET} '
                       '{Style.BRIGHT}{hero.monster_progress}%{Style.RESET_ALL} - '
                       '{news_from_field.msg}',
                       maybe_poi=maybe_poi)

        if self.info.activity_type in ['FIELD', 'TO_TOWN', 'FISHING', 'COOLING']:
            news = self.info.get('news_from_field', {}).get('msg')
            news = news and (' - ' + news) or ''
            self.print('{Fore.GREEN}{activity_type}{Style.RESET_ALL} '
                       '|:{hero.distance}[{near_town_short}]{Fore.BLUE}{maybe_poi}{Fore.RESET} '
                       '{Style.BRIGHT}{hero.s_progress}%{Style.RESET_ALL} {news}',
                       news=news, maybe_poi=maybe_poi)

        if self.info.activity_type == 'FIGHT':
            if 'opponent' in self.info and 'health' in self.info.opponent.hero:
                # abilities (for monster in dungeons for example)
                abilities = ','.join(self.info.opponent.hero.get('ab', []))
                if abilities:
                    abilities = '[{}]'.format(abilities)
                health_percent_str = self.format_health_percent(self.info.opponent_health_percent)
                extra = (
                    ' ENEMY:{opponent.hero.name}{abilities} '
                    '{Fore.RED}HP:{Fore.RESET}'
                    '{Style.BRIGHT}{opponent.hero.health}{Style.NORMAL}/{opponent.hero.max_health}'
                    '{health_percent_str}'
                    .format(Style=colorama.Style, Fore=colorama.Fore, abilities=abilities,
                            health_percent_str=health_percent_str, **self.info)
                )
            elif self.info.hero.get('daura'):
                extra = ' {}'.format(self.info.hero['daura'])
            else:
                extra = ''

            # fight_type variations:
            # "dungeon" for dungeon
            # "monster_m" in dungeon fight
            # "town" for arena
            # "challenge" for train
            # "monster" for some boss monster in field
            # "multi_monster" for some monsters in field
            # "sail" for sail (TODO: fish fight?)
            self.print('{Fore.RED}FIGHT:{Style.BRIGHT}{hero.fight_type}{Style.RESET_ALL} '
                       '{Style.BRIGHT}:{hero.arena_step_count}{Style.NORMAL} '
                       '{hero.turn_progress}% -{extra}', extra=extra)

            self.print_fight_health()

    def print_fight_health(self):
        if not self._fight_health:
            return
        alls_health_percent_str = self.format_health_percent(self.info.alls_health_percent)
        opps_health_percent_str = self.format_health_percent(self.info.opps_health_percent)

        alls_damage = self.format_damage(sum(self._fight_health[-1][2]))
        opps_damage = self.format_damage(sum(self._fight_health[-1][3]))

        def _get_total_damage_max_and_avg(i):
            # filtering healing here
            sums_ = [sum(x for x in r[i] if x > 0) for r in self._fight_health]
            sums_ = [x for x in sums_ if x > 0]
            if not sums_:
                return self.format_damage(0), self.format_damage(0)
            return (self.format_damage(max(sums_)),
                    self.format_damage(sum(sums_) / float(len(sums_))))

        def _get_each_damage_max_and_avg(i):
            # filtering healing here
            flat = []
            for r in self._fight_health:
                flat.extend([x for x in r[i] if x > 0])
            if not flat:
                return self.format_damage(0), self.format_damage(0)
            return (self.format_damage(max(flat)),
                    self.format_damage(sum(flat) / float(len(flat))))

        def _get_damage_stats_str(i):
            max_total_damage, avg_total_damage = _get_total_damage_max_and_avg(i)
            max_each_damage, avg_each_damage = _get_each_damage_max_and_avg(i)
            if (max_total_damage == max_each_damage and avg_total_damage == avg_each_damage):
                return ('MAX:{max_total_damage} '
                        'AVG:{avg_total_damage}'.format(**locals()))
            else:
                return (
                    'MAX-TOTAL:{max_total_damage} AVG-TOTAL:{avg_total_damage} '
                    'MAX-EACH:{max_each_damage} AVG-EACH:{avg_each_damage}'
                    .format(**locals())
                )

        self.print(
            '{Fore.GREEN}ALLS({alls_alive}):{Fore.RESET}{Style.BRIGHT}{alls_health}{Style.NORMAL}'
            '/{alls_max_health}{alls_health_percent_str}{alls_damage}', _get_damage_stats_str(2),
            locals=locals()
        )
        self.print(
            '{Fore.RED}OPPS({opps_alive}):{Fore.RESET}{Style.BRIGHT}{opps_health}{Style.NORMAL}'
            '/{opps_max_health}{opps_health_percent_str}{opps_damage}', _get_damage_stats_str(3),
            locals=locals()
        )

    def print_send_availability(self):
        hero = self.info.hero
        line = ' '.join(filter(None, [
            _get_avail('{Fore.RED}ARENA{Fore.RESET}', hero.arena_send_after,
                       hero.is_arena_available),
            _get_avail('{Fore.GREEN}TRAIN{Fore.RESET}', hero.chfr_after, hero.is_chf_available),
            _get_avail('{Fore.YELLOW}DUNGEON{Fore.RESET}', hero.d_send_after, hero.d_a),
            _get_avail('{Fore.BLUE}SAIL{Fore.RESET}', hero.s_send_after, hero.s_a),
            _get_avail('{Fore.MAGENTA}ROYALE{Fore.RESET}', hero.r_after, hero.r_a),
        ]))
        line and self.print(line)

    def _print_diary_item(self, name, item, now_=None):
        now_ = now_ or now(False)
        delta = now_ - item.time
        if delta.total_seconds() < 0:
            delta = timedelta(seconds=abs(delta.total_seconds()))
            delta = '-{}'.format(delta)
        color = item.get('infl') and colorama.Fore.BLUE or colorama.Fore.WHITE
        color += colorama.Back.BLACK
        print('[{name}] {color}{delta}{Style.RESET_ALL} {item.msg}'
              .format(Style=colorama.Style, **locals()))

    def print_diary(self, **kwargs):
        # info.hero.turn_length: "20",  # this is turn length in seconds
        # so we can calculate ~ time of turn ending
        # and also we know current step_count to expect increasing after that
        arena = kwargs.get('diary_arena', self.params.print_diary_arena)
        log = kwargs.get('diary_log', self.params.print_diary_log)
        important = kwargs.get('diary_important', self.params.print_diary_important)

        now_, info = now(False), self.info
        for i in range(min([arena, len(info.get('arena_fight_log', []))])):
            self._print_diary_item(
                colorama.Back.RED + 'ARN' + colorama.Style.RESET_ALL,
                info.arena_fight_log[i], now_)

        for i in range(min([log, len(info.diary)])):
            self._print_diary_item(
                colorama.Back.BLUE + 'LOG' + colorama.Style.RESET_ALL,
                info.diary[i], now_)
        for i in range(min([important, len(info.imp_e)])):
            self._print_diary_item(
                colorama.Back.YELLOW + 'IMP' + colorama.Style.RESET_ALL,
                info.imp_e[i], now_)

    def print_info(self, **kwargs):
        print('---------------------------------------------')
        if kwargs.get('hero', self.params.print_hero):
            self.print_hero()
        if kwargs.get('activity', self.params.print_activity):
            self.print_activity()
        self.print_diary(**kwargs)
        if (kwargs.get('send_availability', self.params.print_send_availability) and
           not self.info.hero.arena_fight):
            self.print_send_availability()


def print_help(settings):
    import inspect
    from hero_action import Condition, conditions_registry, HeroAction
    from hero_utils import get_action_registry

    action_registry = get_action_registry(settings.action_modules)

    print('ACTIONS:')
    for name, cls in action_registry.items():
        print('  ' + name)
        for k, v in cls.PARAMS.items():
            if v is inspect._empty:
                v = '(required)'
            print('    ' + k + ': ' + str(v))
        for k, v in cls.CONDITIONS.items():
            if callable(v):
                v = '(dynamic)'
            print('    ' + k + ': ' + str(v))

    print('ACTION PARAMS:')
    sig = inspect.signature(HeroAction.__init__)
    for k, v in tuple(sig.parameters.items())[2:-1]:
        print('  ' + k + ': ' + str(v.default))

    print('ACTION CONDITIONS:')
    print('  STR_ATTRS: ' + ' '.join(Condition.STR_ATTRS))
    print('  NUM_ATTRS: ' + ' '.join(Condition.NUM_ATTRS))
    print('  KEYS: ' + ' '.join(conditions_registry.keys()))
