#!/usr/bin/env python

import logging
import time
import random
from datetime import timedelta
from lxml import html

from requests.exceptions import ConnectionError, Timeout
from god_info import god_info
from hero_api import HeroApi
from hero_print import HeroPrint, print_help
from hero_info import HeroInfo
from hero_database import HeroDatabase
from hero_utils import (pprint, now, load_settings, configure_logging, run_ipython, playsound,
                        get_changed_keys, get_action_registry)


class HeroFightInfo:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._fight_started = None
        self._fight_finished = None
        self._fight_health = []

    def _update_fight_info(self):
        if not self.info.get('opps'):
            if self._fight_started:
                self._fight_finished = now()
            if self._fight_health:
                self._fight_health = []
            return

        if not self._fight_started or self._fight_finished:
            self._fight_started = now()
            self._fight_finished = None
            self._fight_health = []

        alls_hp = [h.hp for h in self.info.alls] + [self.info.hero.health]
        opps_hp = [h.get('health', h.get('hp')) for h in self.info.opps]
        assert None not in opps_hp

        if self._fight_health:
            def _get_deltas(cur, prev):
                if len(cur) > len(prev):
                    # in case of adding enemies in battle
                    prev = prev + cur[len(prev):]

                elif len(cur) < len(prev):
                    # TODO: have this while sail fish fight
                    # raise AssertionError('len(%s) < len(%s)' % (cur, prev))
                    # TODO: is it right? just reversed adding enemies in battle logic
                    cur = cur + prev[len(cur):]

                return list(map(lambda xy: xy[0] - xy[1], zip(prev, cur)))
            alls_hp_, opps_hp_ = self._fight_health[-1][:2]
            alls_deltas = _get_deltas(alls_hp, alls_hp_)
            opps_deltas = _get_deltas(opps_hp, opps_hp_)
        else:
            alls_deltas = [0 for _ in alls_hp]
            opps_deltas = [0 for _ in opps_hp]

        self._fight_health.append([alls_hp, opps_hp, alls_deltas, opps_deltas])


class HeroMap:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._map = None
        self._map_updated = None

    def _set_info_map(self, info):
        if (
            not self._map_updated or
            info.activity_type in ['FIELD'] or
            (info.ctime - self._map_updated) > timedelta(minutes=10)
        ):
            self._map = self.get_map()
            self._map_updated = info.ctime
        info._set_map(self._map)


class Hero(HeroFightInfo, HeroMap, HeroPrint, HeroApi, HeroDatabase):
    def __init__(self, username, password, action_registry,
                 params={}, constants={}, voices={}, inventory_types={},
                 forecasts={}):
        self.username = username
        self.password = password
        self.action_registry = action_registry

        self.info = None
        self._voice_next_time = None

        self.params = params
        self.constants = constants
        self.voices = voices
        self.inventory_types = inventory_types
        self.forecasts = forecasts

        self.logger = logging.getLogger('hero')

        super().__init__()

    @property
    def debug(self):
        return self.params.get('debug', False)

    def __call__(self, *args, **kwargs):
        return self.run(*args, **kwargs)

    def alert(self, path=None, shell=None):
        playsound(path or self.params.alert_path, shell or self.params.get('alert_shell'))

    def sleep(self, seconds=None, till=None, reason=None, allow_skip=True):
        if seconds is None and till is None:
            raise ValueError('seconds or till argument required')
        if till:
            seconds = (till - now()).total_seconds()
        else:
            till = now() + timedelta(seconds=seconds)
        if seconds <= 0:
            if not allow_skip:
                raise RuntimeError('Sleep in past is not allowed')
            # TODO: this is the reason for forever request on some error
            # and godville will ban IP for some time...
            self.logger.warning('Sleep skipped %.2f till %s: %s', seconds, till,
                                reason or '-')
            return
        if reason:
            self.logger.info('Sleep %.2f till %s: %s', seconds, till, reason or '-')
        time.sleep(seconds)

    def voice(self, voice):
        if isinstance(voice, str) and voice in self.voices:
            voice = self.voices[voice]
        if isinstance(voice, dict) and 'voices' in voice:
            voice = random.choice(voice.voices)
        if not isinstance(voice, str):
            raise ValueError('Voice must be string or dict with voices key')
        rv = super().voice(voice)
        return rv

    def _create_action(self, action):
        kwargs = action.copy()
        name = kwargs.pop('action')
        if name not in self.action_registry:
            raise ValueError('Unknown action: %s' % name)
        if True in kwargs:
            # this is "on" key, translated by yaml to True
            kwargs['on'] = kwargs.pop(True)
        self.logger.debug('Creating action %s: %s', name, kwargs)
        return self.action_registry[name](self, **kwargs)

    def _create_actions(self, actions):
        if isinstance(actions, dict):
            for k, v in actions.items():
                v['action'] = k
            actions = tuple(actions.values())
        return [self._create_action(a) for a in actions]

    def _fetch_info(self):
        while True:
            try:
                info = HeroInfo(self.get_info(), self.constants)
                self._set_info_map(info)
                return info
            except (ConnectionError, Timeout) as exc:
                if not self.params.connection_error_sleep:
                    raise
                self.sleep(self.params.connection_error_sleep,
                           reason='Waiting connection on error: {}'.format(exc))

    def _run(self, info, actions, **kwargs):
        print_info = kwargs.pop('print_info', self.params.print_info)
        diary_log = kwargs.pop('diary_log', self.params.diary_log)
        print_kwargs = {k[6:]: kwargs.pop(k) for k in kwargs
                        if k.startswith('print_')}
        if kwargs:
            raise TypeError('Unknown arguments: %s' % kwargs)

        prev_info, self.info = self.info, info

        self._update_fight_info()
        if print_info:
            self.print_info(**print_kwargs)
        if diary_log:
            self.update_diary_log()

        events = None
        if prev_info:
            events = (get_changed_keys(prev_info, self.info) |
                      get_changed_keys(prev_info.hero, self.info.hero))
            if self.debug:
                self.logger.debug('Events: %s', events)

        for action in actions:
            if events is None or 'update' in action.on or events.intersection(action.on):
                stop_propagation = action()
                if stop_propagation:
                    break

    def run(self, actions, **kwargs):
        monitor_sleep = kwargs.pop('monitor_sleep', self.params.monitor_sleep)
        actions = self._create_actions(actions)

        info = self._fetch_info()
        while True:
            try:
                self._run(info, actions, **kwargs)
            except (ConnectionError, Timeout) as exc:
                if not self.params.connection_error_sleep:
                    raise
                self.sleep(self.params.connection_error_sleep,
                           reason='Waiting connection on error: {}'.format(exc))
                info = self._fetch_info()
                continue

            if not monitor_sleep:
                break

            if info.hero.arena_fight:
                step = info.hero.arena_step_count
                while True:
                    # this is how it should be
                    sleep_time = (float(info.hero.turn_length) *
                                  (float(1) - (float(info.hero.turn_progress) / 100)))
                    # this is reality
                    sleep_time += 2
                    self.sleep(sleep_time, reason='Next turn({})'.format(step + 1))
                    info = self._fetch_info()
                    if (not info.hero.arena_fight or
                       info.hero.arena_step_count != step):
                        # assert self.info.hero.arena_step_count > step
                        # TODO: may be switching from another fight type,
                        # for example in dungeon
                        break
            else:
                now_ = now()
                till = now_ + timedelta(seconds=monitor_sleep)
                if self._voice_next_time and self._voice_next_time > now_:
                    till = min([self._voice_next_time, till])
                self.sleep(till=till, reason='Monitor', allow_skip=False)
                info = self._fetch_info()

    def gp_cap_monitor(self, needed=None, sleep=3):
        max_values = []
        prev_value = None
        while True:
            value, timestamp, already_used = self.gp_cap_check()
            if already_used:
                self.logger.error('GP CAP ALREADY USED')
                return False
            # We use (needed - 1) because we get 10 when value is > 9
            if needed and value > (needed - 1):
                res = self.gp_cap_use(timestamp)
                self.logger.info(res)
                return res
            else:
                if prev_value and prev_value > value:
                    max_values.append(prev_value)
                    self.logger.info('GP CAP reset: %.2f MAX:%.2f AVG:%.2f',
                                     prev_value, max(max_values),
                                     sum(max_values) / float(len(max_values)))
                prev_value = value
                sleep_ = 0.5 if (needed and (value > (needed - 2))) else sleep
                self.sleep(sleep_, reason=None)

    def resolve_cross(self):
        rv = self.session.get('https://gv.erinome.net/db?cross=current&lang=ru')
        doc = html.fromstring(rv.content)
        data = {}
        for e in doc.cssselect('table.cross td.cell'):
            char = e.cssselect('div.open')[0].text_content()
            id = e.attrib['id']
            assert id[0] == 'c'
            # normalize id
            y, x = int(id[1:len(id) - 1] or 0), int(id[-1])
            if y % 2:
                x += 10
                y -= 1
            y = int(y / 2)
            data[(x, y)] = char
        assert data, 'No resolved cross found'
        rv = self.cross_check(data, raise_error=True)
        return rv['status']

    def god_info(self):
        info = self._fetch_info()
        return god_info(info.hero.godname)

    def get_untrained_info(self):
        trained_heroes = self.get_trained_heroes()['a']
        friends = self.get_friends()['friends']
        self.logger.info('Trained: %s/%s', len(trained_heroes), len(friends))
        return ['%s :: %s' % (d['name'], d['heroname']) for d in friends
                if d['heroname'] not in trained_heroes]

    def get_trained_info(self):
        trained_heroes = self.get_trained_heroes()['a']
        friends = self.get_friends()['friends']
        self.logger.info('Trained: %s/%s', len(trained_heroes), len(friends))
        return ['%s :: %s' % (d['name'], d['heroname']) for d in friends
                if d['heroname'] in trained_heroes]


def create_hero(settings, run=False):
    return Hero(settings.auth.username, settings.auth.password,
                get_action_registry(settings.action_modules),
                params=settings.params, constants=settings.constants, voices=settings.voices,
                inventory_types=settings.inventory_types)


if __name__ == '__main__':
    import sys
    configs = [arg for arg in sys.argv[1:] if not arg.startswith('--')]
    flags = [arg[2:] for arg in sys.argv[1:] if arg.startswith('--')]
    if 'pdb' in flags or 'ipython' in flags:
        flags.append('interactive')

    def reload():
        from hero import create_hero
        context['settings'] = load_settings(*configs)
        configure_logging(context['settings'])
        context['hero'] = create_hero(context['settings'])

    def run(reload_=False, actions=True):
        reload_ and reload()
        context['hero'](context['settings'].actions if actions else [])

    context = {}

    if 'help' in flags:
        print_help(load_settings(*configs))

    elif 'gp-cap-monitor' in [f.split('=')[0] for f in flags]:
        # TODO: use click library sometime
        reload()
        try:
            needed = [int(f.split('=')[1]) for f in flags
                      if f.split('=')[0] == 'gp-cap-monitor'][0]
        except IndexError:
            needed = None
        try:
            context['hero'].gp_cap_monitor(needed)
        except KeyboardInterrupt:
            pass

    elif 'god-info' in flags:
        reload()
        pprint(context['hero'].god_info())

    elif 'resolve-cross' in flags:
        reload()
        pprint(context['hero'].resolve_cross())

    elif 'collect-coupon' in flags:
        reload()
        pprint(context['hero'].collect_coupon())

    elif 'untrained-info' in flags:
        reload()
        pprint(context['hero'].get_untrained_info())

    elif 'trained-info' in flags:
        reload()
        pprint(context['hero'].get_trained_info())

    elif 'interactive' not in flags:
        print('Non-interactive mode. CTRL+C to stop.')
        print('Python', sys.version.split('\n')[0].strip(), 'on', sys.platform)
        try:
            if 'monitor' in flags:
                run(True, actions=False)
            else:
                # "--run" by default
                run(True)

        except KeyboardInterrupt:
            pass
    else:
        print('Interactive mode. CTRL+C to stop, CTRL+D to exit shell, '
              'run() to continue, run(True) to reload and continue.')
        context['reload'] = reload
        context['run'] = run
        if 'run' in flags:
            run_ipython(context, 'run(True)', pdb='pdb' in flags)
        elif 'monitor' in flags:
            run_ipython(context, 'run(True, actions=False)', pdb='pdb' in flags)
        else:
            run_ipython(context, 'reload()', pdb='pdb' in flags)
