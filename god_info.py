#!/usr/bin/env python

import re
from datetime import datetime, timedelta
from urllib.parse import quote

from lxml import html
import requests
from dateutil.relativedelta import relativedelta


def _get_parentheses(data):
    return re.match('.*\((.*)\)$', data).groups(1)[0]


def _percent_to_count(data):
    return int(data.replace('.', '').replace('%', ''))


def _parse_birthday_delta(data):
    match = re.match('(\d+) (года?|лет)( (\d+) месяц)?', data)
    if match:
        years = int(match.group(1))
        months = match.group(4) and int(match.group(4)) or 0
        return relativedelta(years=years, months=months)
    match = re.match('(\d+) месяц\w*( (\d+) д)?', data)
    if match:
        months = int(match.group(1))
        days = match.group(3) and int(match.group(3)) or 0
        print(months, days)
        return relativedelta(months=months) + timedelta(days=days)
    raise AssertionError('Uhknown delta: {}'.format(data))


def _parse_birthday(data, now=datetime.utcnow):
    # OK, we don't know timezone, so counting related to utc
    return (now() if callable(now) else now) - _parse_birthday_delta(data)


def _calculate_stats(since, value, end_value=1000, start_value=0, now=datetime.utcnow):
    # OK, we don't know timezone, so counting related to utc
    now = now() if callable(now) else now
    seconds = (now - since).total_seconds()
    per_second = (value - start_value) / seconds
    per_day = per_second * 60 * 60 * 24
    total_days = ((end_value - start_value) / per_second) / (60 * 60 * 24)
    eta = since + timedelta(seconds=((end_value - start_value) / per_second))
    left_days = (eta - now).total_seconds() / (60 * 60 * 24)
    return {
        'start_at': since,
        'end_at': eta,
        'days_total': total_days,
        'days_left': left_days,
        'days_past': total_days - left_days,
        'value_start': start_value,
        'value_end': end_value,
        'value_current': value,
        'per_day': per_day,
    }


def god_info(godname):
    url = 'https://godville.net/gods/{}'.format(quote(godname))
    resp = requests.get(url, timeout=10)
    resp.raise_for_status()
    tree = html.fromstring(resp.content)

    common = {}
    _fix_tr_name = None
    for tr in tree.cssselect('table#characteristics')[0]:

        # fix for forgotten <tr> tag...
        if tr.tag == 'td':
            if tr.text in ['Сбережения', 'Лавка']:
                _fix_tr_name = tr.text
                continue
            else:
                assert _fix_tr_name, tr.text_content()
                common[_fix_tr_name] = tr.text
                _fix_tr_name = None
                continue

        name, value = tr[0].text, tr[1].text_content().strip()
        if name == 'Побед / Поражений':
            name = name.split(' / ')
            value = value.strip().split(' / ')
            common[name[0]] = int(value[0])
            common[name[1]] = int(value[1])
            continue
        elif name == 'Гильдия':
            if value == 'не состоит':
                value = None
            else:
                guild_name, guild_rank = value[:-1].rsplit(' (', 1)
                common[name] = guild_name.strip()
                common[name + '[rank]'] = guild_rank.strip()
                continue
        elif name == 'Смертей':
            value = int(value)
        common[name] = value

    common['birth_at'] = _parse_birthday(common['Возраст'])

    awards = {}  # Храмовладелец # Корабел # Зверовод # Тваревед # Лавочник
    for span in tree.cssselect('div.t_award_d span.t_award_dt'):
        try:
            name, since = span.text.split(' с ')
        except ValueError:
            # now try latin "c" (hello, godville programmers!)
            name, since = span.text.split(' c ')
        awards[name] = datetime.strptime(since, '%d.%m.%Y %H:%M')

    stats = {}

    gold = _percent_to_count(common.get('Кирпичей для храма', '0'))
    if 'Храмовладелец' in awards:
        gold = 1000
    if gold:
        till = datetime.utcnow()
        if 'Храмовладелец' in awards:
            till = awards['Храмовладелец']
        stats['gold'] = _calculate_stats(common['birth_at'], gold, 1000, now=till)

    wood = _percent_to_count(common.get('Дерева для ковчега', '0'))
    if 'Ковчег достроен' in common:
        wood = _percent_to_count(_get_parentheses(common['Ковчег достроен']))
    if wood:
        if 'Корабел' in awards:
            since = awards['Корабел']
            next_value = wood + (1000 - (wood % 1000))
            start_value = 1000
        else:
            since = awards['Храмовладелец']
            next_value = 1000
            start_value = 0
        stats['wood'] = _calculate_stats(since, wood, next_value, start_value)

    if 'Сбережения' in common:
        pension = int(common['Сбережения'].split()[0])
        if pension:
            since = max(awards['Храмовладелец'], datetime(2012, 7, 18))
            stats['pension'] = _calculate_stats(since, pension, 30000)

    if 'Твари по паре' in common:
        animals = _percent_to_count(_get_parentheses(common['Твари по паре']))
        if animals:
            stats['animals'] = _calculate_stats(awards['Корабел'], animals)

    return common, awards, stats


if __name__ == '__main__':
    import sys
    from hero_utils import pprint

    if not len(sys.argv) >= 2:
        sys.exit('Please pass GODNAME as first argument')
    godname = ' '.join(sys.argv[1:])
    print(godname)
    pprint(god_info(godname))
