from datetime import timedelta, datetime
from collections import defaultdict

from hero_action import HeroAction, ConditionFail
from hero_info import HeroInfo
from hero_utils import now
from dateutil.parser import parse as dateutil_parse


class Alert(HeroAction):
    def _call(self):
        self.alert = True


class Resurrect(HeroAction):
    ON = ['health']

    def _call(self):
        if self.info.hero.health == 0 and not self.info.hero.arena_fight:
            self.hero.resurrect()
        else:
            raise ConditionFail('Not dead')


class Punish(HeroAction):
    CONDITIONS = {'godpower_min': lambda h: h.constants.punish_needs_godpower}

    def _call(self):
        self.hero.punish()


class Encourage(HeroAction):
    CONDITIONS = {'godpower_min': lambda h: h.constants.encourage_needs_godpower}

    def _call(self):
        self.hero.encourage()


class Miracle(HeroAction):
    CONDITIONS = {'godpower_min': lambda h: h.constants.miracle_needs_godpower}

    def _call(self):
        self.hero.miracle()


class AccRestore(HeroAction):
    CONDITIONS = {'accumulator_min': 1}

    def _call(self):
        self.hero.acc_restore()


class AccAccumulate(HeroAction):
    CONDITIONS = {'godpower_min': 100}

    def _call(self):
        self.hero.acc_restore()


class TrainAccept(HeroAction):
    ON = ['chf_pending']

    def _call(self):
        god_name = self.info.hero.get('chf_pending')
        if god_name:
            self.hero.accept_train(god_name)
            return True  # stop propagation
        else:
            raise ConditionFail('No train pending')


class Voice(HeroAction):
    CONDITIONS = {'godpower_min': lambda h: h.constants.voice_needs_godpower}

    def __init__(self, hero, voice, **kwargs):
        if isinstance(voice, str) and voice in hero.voices:
            voice = hero.voices[voice]
        if isinstance(voice, dict) and 'voices' in voice:
            conditions = voice.copy()
            conditions.pop('voices')
            for k, v in conditions.items():
                kwargs.setdefault(k, v)
        super().__init__(hero, voice=voice, **kwargs)

    def _call(self, voice):
        if (not self.info.hero.arena_fight and self.hero._voice_next_time and
           self.hero._voice_next_time > now()):
            raise ConditionFail('next time in future: %s' % self.hero._voice_next_time)
        self.hero.voice(voice)

        for _ in range(3):
            info = HeroInfo(self.hero.get_info(), self.hero.constants)
            infl, resp = self._find_diary_voice_influence(info)
            if infl:
                break
            self.hero.sleep(2, reason='Waiting influence in diary')
        if not infl:
            self.logger.warning('Voice sent, influence not found')
            # raise RuntimeError('Voice sent, influence not found')

        if not infl or resp:
            delta = timedelta(seconds=self.hero.params.voice_response_timeout)
        else:
            delta = timedelta(seconds=self.hero.params.voice_no_response_timeout)
        infl_time = infl and infl.time or now()
        infl_msg = infl and infl.msg or '-'
        self.hero._voice_next_time = (now() > infl_time and now() or infl_time) + delta
        return 'Voice influence %s: %s: %s' % (infl_time, infl_msg, resp and resp.msg or '-')

    def _find_diary_voice_influence(self, info):
        last_diary = [log for log in self.info.diary if 'vote_id' not in log][0]
        infl, resp = None, None

        for i, log in enumerate(info.diary):
            if log.time == last_diary.time and log.msg == last_diary.msg:
                # We can't rely on time because new influences may be
                # actually older, but appears after last diary
                break
            if log.get('infl'):
                if infl:
                    self.logger.error('"infl" found more than one: {}'.format(log))
                    # TODO: for some reason we can have this error, no time for debug
                    # assert not infl, '"infl" found more than one: {}'.format(log)
                infl = log
                if i and (info.diary[i-1].time - log.time).total_seconds() == 10:
                    resp = info.diary[i-1]
        return infl, resp


class Heal(HeroAction):
    ON = ['health', 'godpower', 'fight_type']

    # NOTE: by default not working on arena, because it may be zpg!
    # Also not working by default in dungeon corridors because it may be not effective
    CONDITIONS = {'activity_type_in': ['FIGHT'],
                  'fight_type_in': ['monster', 'monster_m', 'multi_monster']}

    def __init__(self, hero, voice=False, encourage=False, **kwargs):
        if not voice and not encourage:
            raise ValueError('voice or encourage must be supplied')
        if not voice:
            kwargs.setdefault('godpower_min', hero.constants.encourage_needs_godpower)
        else:
            kwargs.setdefault('godpower_min', hero.constants.voice_needs_godpower)
        super().__init__(hero, voice=voice, encourage=encourage, **kwargs)

    def _call(self, voice=False, encourage=False, check_abilities=True):
        if 'opponent' in self.info and 'hero' in self.info.opponent:
            ab = [ab_.lower() for ab_ in self.info.opponent.hero.get('ab', [])]
        else:
            ab = []

        if encourage and self.info.hero.godpower >= self.hero.constants.encourage_needs_godpower:
            if (check_abilities and voice and 'паразитирующий' in ab
               and 'глушащий' not in ab and 'ушастый' not in ab):
                self.logger.debug('Skipping encourage because of opponent abilities: %s', ab)
            else:
                self.hero.encourage()
                return 'encouraged'
        if voice:
            if check_abilities and 'глушащий' in ab:
                self.logger.debug('Skipping voice because of opponent abilities: %s', ab)
            else:
                self.hero.voice(self.hero.voices.heal)
                return 'voice sent'


class DiaryVoiceVote(HeroAction):
    ON = ['diary']
    CONDITIONS = {'godpower_max': 99}

    def __init__(self, *args, **kwargs):
        self._voted = []
        super().__init__(*args, **kwargs)

    def _call(self):
        voted = []
        for log in self.info.diary:
            if log.get('voice') and log.get('voice_id'):
                if not log['voice_id'] in self._voted:
                    self.hero.diary_voice_vote(log['voice_id'], True)
                    voted.append(log['voice_id'])
        if voted:
            self._voted.extend(voted)
            self._voted = self._voted[-10:]
            return 'Voted: %s' % voted
        raise ConditionFail('Nothing to vote')


class InventoryActivate(HeroAction):
    ON = ['inventory', 'gp', 'activity_type']
    CONDITIONS = {'activity_type_in': ['FIELD', 'TOWN', 'TO_TOWN']}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.type_map = {v: k for k, v in self.hero.inventory_types.items()}

    def _call(self, names=[], types=[], needs_gp_lte=None):
        activated = []
        for name, info in self.info.inventory_active.items():
            type_ = self.type_map.get(info.get('description'))
            if (name in names or (type_ and type_ in types)):
                if self._activate(name, info, needs_gp_lte):
                    activated.append(name)
        if activated:
            return 'Activated: %s' % activated
        raise ConditionFail('Nothing to activate')

    def _activate(self, name, info, needs_gp_lte):
        needs_gp = info.get('needs_godpower', 0)
        if needs_gp_lte is not None and needs_gp > needs_gp_lte:
            self.logger.warning('needs_gp(%s) > needs_gp_lte(%s) for %s',
                                needs_gp, needs_gp_lte, name)
            return False
        elif self.info.hero.godpower >= needs_gp:
            self.hero.inventory_activate(name)
            return True
        else:
            self.logger.warning('Not enough godpower for %s', name)
            return False


class _SendToArena(HeroAction):
    CONDITIONS = {
        'godpower_min': lambda h: h.constants.send_to_arena_needs_godpower,
        'level_min': 10,
    }

    def _call(self):
        if not (
            not self.info.hero.arena_fight and
            self.info.hero.is_arena_available and
            not self.info.hero.arena_send_after
        ):
            raise ConditionFail('Arena not available')

    def send_to_pvp_arena(self):
        # Looks like "ареналин" doesn't work with ZPG
        if 'ареналин' in self.info.inventory:
            self.hero.inventory_activate('ареналин')
        else:
            self.hero.send_to_pvp_arena()

    def is_yarena_time(self):
        return bool(
            (
                self.info.ctime.hour in self.constants.yarena_daily_hours and
                self.info.ctime.minute in self.constants.yarena_daily_minutes
            ) or (
                self.info.ctime.day % 2 == 0 and
                self.info.ctime.hour in self.constants.yarena_even_day_hours and
                self.info.ctime.minute in self.constants.yarena_even_day_minutes
            )
        )

    def is_zpg_time(self):
        return (self.info.ctime.minute in self.constants.zpg_arena_minutes)


class SendToYarena(_SendToArena):
    # https://wiki.godville.net/%D0%AF%D1%80%D0%B5%D0%BD%D0%B0

    def _call(self):
        super()._call()
        if not self.is_yarena_time():
            raise ConditionFail('Not yarena time')

        self.send_to_pvp_arena()


class SendToZpgArena(_SendToArena):
    def _call(self, skip_yarena_daily_hours=False):
        super()._call()
        if not self.is_zpg_time():
            raise ConditionFail('Arena is not ZPG')
        if (
            skip_yarena_daily_hours and
            self.info.ctime.hour in self.constants.yarena_daily_hours
        ):
            raise ConditionFail('Skip yarena daily hours')

        # Looks like "ареналин" doesn't work with ZPG
        if not self.hero.send_to_zpg_arena():
            raise ConditionFail('Arena is not ZPG (send failed)')


class SendToDungeon(HeroAction):
    CONDITIONS = {'godpower_min': 50}

    def __init__(self, *args, **kwargs):
        self._wood_avail = None
        super().__init__(*args, **kwargs)

    def _call(self, check_wood=True):
        if not (
            not self.info.hero.arena_fight and
            self.info.hero.d_a and
            not self.info.hero.d_send_after
        ):
            raise ConditionFail('Dungeon not available')

        if check_wood:
            if not self._wood_avail:
                self._set_wood_avail()
            if self._wood_avail is not True and self._wood_avail > now():
                raise ConditionFail('Wood available in future: {}'.format(self._wood_avail))
            # check last time RIGHT before sending
            self._set_wood_avail()
            if self._wood_avail is not True:
                raise ConditionFail('Wood not available ({})'.format(self._wood_avail))

        self.hero.send_to_dungeon()

    def _set_wood_avail(self):
        self._wood_avail = self.hero.check_dungeon_wood()
        if self._wood_avail is not True:
            self._wood_avail = now() + self._wood_avail


class SendToRoyale(HeroAction):
    CONDITIONS = {'godpower_min': 50}

    def _call(self):
        if not (
            not self.info.hero.arena_fight and
            self.info.hero.r_a and
            not self.info.hero.r_after
        ):
            raise ConditionFail('Royale not available')

        self.hero.send_to_royale()


class SendToSail(HeroAction):
    CONDITIONS = {'godpower_min': 50}

    def _call(self):
        if not (
            not self.info.hero.arena_fight and
            self.info.hero.s_a and
            not self.info.hero.s_send_after
        ):
            raise ConditionFail('Sail not available')

        self.hero.send_to_sail()


def fstr_format(template, context):
    return eval(f"f\"{template}\"", globals(), context)


class Chat(HeroAction):
    ON = ['m_id', 'm_id_n']
    _max_sent_at = ''
    _asterisk_timeouts = {}  # TODO: change it to __init__

    def _set_max_sent_at(self, friends):
        if friends:
            self._max_sent_at = max(
                (f.get('msg') and dateutil_parse(f['msg']['sent_at']) or '')
                for f in friends
            )

    def _call(self, chat={}, asterisk_exclude_gods=[], asterisk_timeout=60):
        friends = self.hero.get_friends()['friends']
        if not self._max_sent_at:
            self._set_max_sent_at(friends)
        sent = []
        for f in friends:
            # if f['name'] in []:
            #     print('friend =', f, self._max_sent_at)
            if (
                f.get('msg')
                and f['msg']['from'] == f['name']
                and dateutil_parse(f['msg']['sent_at']) > self._max_sent_at
            ):
                for q, a in chat.items():
                    if (
                        q == f['msg']['msg']
                        or (
                            q == '*' and f['name'] not in asterisk_exclude_gods
                            and (
                                datetime.utcnow() -
                                self._asterisk_timeouts.get(f['name'], datetime(1970, 1, 1))
                                ).total_seconds() > asterisk_timeout
                        )
                    ):
                        if q == '*':
                            self._asterisk_timeouts[f['name']] = datetime.utcnow()
                        msg = fstr_format(a, {**self.hero.info, **self.hero.info.hero})
                        self.hero.send_chat(f['name'], msg)
                        sent.append(f['name'])
                        break
        self._set_max_sent_at(friends)
        if sent:
            return 'Sent: ' + ','.join(sent)
        raise ConditionFail('No known messages')


class LaboratoryRemove(HeroAction):
    ON = ['inventory', 'activity_type']
    CONDITIONS = {'activity_type_in': ['FIELD', 'TOWN', 'TO_TOWN']}

    def _call(self):
        parts = defaultdict(lambda: defaultdict(list))
        for name, info in self.info.inventory.items():
            if ' босса ' in name:
                part_name, _, boss_name = name.split(' ', 2)
                boss_level = self.constants.boss_levels.get(boss_name, 1)
                parts[part_name][boss_level].append(boss_name)
        if not parts:
            raise ConditionFail('Nothing for laboratory in inventory')
        raise NotImplementedError()
