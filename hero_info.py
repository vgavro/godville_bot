from functools import reduce
from datetime import datetime, timezone
import re

from dateutil.parser import parse as dateutil_parse
from dateutil.tz import gettz

from hero_utils import AttrDict

MSK = gettz('Europe/Moscow')

wrd_regexp = re.compile(r'([\d\.]+)% \+ \[[^\.]*(\.+)\]')


def wrd_cnt(wrd):
    match = wrd_regexp.match(wrd)
    if not match:
        # print('unmatched', wrd)
        return 0
    words, syllable_rest = match.groups()
    return (float(words) * 10) + (0.25 * (4 - len(syllable_rest)))


def _parse_item_time(item, time_key='time'):
    dt = dateutil_parse(item[time_key])
    assert dt.tzinfo is not None
    item[time_key] = dt.astimezone(MSK)
    return item


def _create_item_time(item, time_key='time'):
    item[time_key] = datetime.now(timezone.utc).astimezone(MSK)
    return item


class HeroInfo(AttrDict):
    def __init__(self, data, constants):
        super().__init__(data)
        self.constants = constants

        if not hasattr(self.__class__, '_fishing_regexp'):
            self.__class__._fishing_regexp = re.compile('|'.join(constants.fishing_regexp))

        assert self.hero.max_gp == 100

        if 'ctime' in self:
            _parse_item_time(self, 'ctime')
        else:
            # well, for some reason we don't have ctime in response while in boss battle "royale"
            _create_item_time(self, 'ctime')

        if 'news_from_field' in self:
            _parse_item_time(self.news_from_field)
        if 'arena_fight_log' in self:
            self['arena_fight_log'] = [_parse_item_time(log)
                                       for log in self['arena_fight_log']]
        self['diary'] = [_parse_item_time(log)
                         for log in self.get('diary', [])]
        self['imp_e'] = [_parse_item_time(log)
                         for log in self.get('imp_e', [])]
        if 'gc_a' in self:
            # achievements
            self['gca'] = self.pop('gc_a')

        for attr in dir(self.__class__):
            if (not attr.startswith('_') and
               attr not in self.keys() and
               isinstance(getattr(self.__class__, attr), property)):
                self[attr] = getattr(self, attr)

    def _set_map(self, map_):
        near_town = None
        next_town = None
        for point in sorted([int(k) for k in map_['t']], reverse=True):
            if self.hero.distance >= point:
                near_town = map_['t'][str(point)]
                break
            else:
                next_town = map_['t'][str(point)]
        self['near_town'] = near_town
        self['near_town_desc'] = map_['d'].get(near_town) if near_town else None
        self['near_town_short'] = map_['c'][near_town] if near_town else None
        self['next_town'] = next_town
        self['next_town_desc'] = map_['d'].get(next_town) if next_town else None
        self['next_town_short'] = map_['c'][next_town] if next_town else None

    def _get_dotted(self, key):
        return reduce(lambda d, k: d.get(k) if d else None, key.split('.'), self)

    @property
    def health_percent(self):
        return (self.hero.health / self.hero.max_health) * 100

    @property
    def lab_f(self):
        return abs(self['hero'].get('bp_f', 0))

    @property
    def lab_m(self):
        return abs(self['hero'].get('bp_m', 0))

    @property
    def inventory_percent(self):
        return (self.hero.inventory_num / self.hero.inventory_max_num) * 100

    @property
    def inventory_active(self):
        return{k: v for k, v in self.inventory.items()
               if v.get('activate_by_user')}

    @property
    def inventory_active_cnt(self):
        return sum(i.cnt for i in self.inventory.values()
                   if i.get('activate_by_user'))

    @property
    def inventory_bold_cnt(self):
        return sum(i.cnt for i in self.inventory.values()
                   if i.price and not i.get('activate_by_user'))

    @property
    def inventory_heal_cnt(self):
        return sum(i.cnt for i in self.inventory.values()
                   if i.get('type') == 'heal_potion')

    @property
    def inventory_common_cnt(self):
        return (self.hero.inventory_num - self.inventory_active_cnt -
                self.inventory_bold_cnt - self.inventory_heal_cnt)

    @property
    def opponent_health_percent(self):
        if 'opponent' in self and 'health' in self.opponent.hero:
            return (self.opponent.hero.health / self.opponent.hero.max_health) * 100

    @property
    def opps_real(self):
        return [
            o for o in self.get('opps', [])
            # Filtering self (because of sailing)
            if o.get('god') != self.hero.godname and o.get('hero') != self.hero.name
        ]

    @property
    def opps_health(self):
        if self.opps_real:
            return sum(h.get('health', h.get('hp')) for h in self.opps_real)

    @property
    def opps_max_health(self):
        if self.opps_real:
            return sum(h.get('max_health', h.get('hpm')) for h in self.opps_real)

    @property
    def opps_health_percent(self):
        if self.opps_real:
            return (self.opps_health / self.opps_max_health) * 100

    @property
    def opps_alive(self):
        min_hp = self.hero.get('fight_type') == 'town' and 1 or 0
        return sum(1 for h in self.opps_real
                   if h.get('health', h.get('hp')) > min_hp)

    @property
    def alls_health(self):
        if self.hero.get('fight_type') == 'sail':
            return self.hero.health
        return sum(h.hp for h in self.get('alls', [])) + self.hero.health

    @property
    def alls_max_health(self):
        if self.hero.get('fight_type') == 'sail':
            return self.hero.max_health
        return sum(h.hpm for h in self.get('alls', [])) + self.hero.max_health

    @property
    def alls_health_percent(self):
        return (self.alls_health / self.alls_max_health) * 100

    @property
    def alls_alive(self):
        if self.hero.get('fight_type') == 'sail':
            return self.hero.health > 1 and 1 or 0
        return (sum(1 for h in self.get('alls', []) if h.hp > 1) +
                (self.hero.health > 1 and 1 or 0))

    def get_activity_type(self):
        if self.hero.arena_fight:
            return 'FIGHT'
        if self.hero.get('town_name'):
            return 'TOWN'
        if self.hero.monster_name:
            return 'MONSTER'
        if self.hero.dir == 'tt':
            return 'TO_TOWN'
        if self.field_msg in ['Переводит дух...', 'Сушит вещи...']:
            return 'COOLING'
        # NOTE: we can't filter when hero is healing or fishing
        if self._fishing_regexp.search(self.hero.get('diary_last', '')):
            return 'FISHING'
        return 'FIELD'

    @property
    def activity_type(self):
        if '_activity_type' not in self.__dict__:
            self._activity_type = self.get_activity_type()
        return self._activity_type

    @property
    def pet_level(self):
        # -1 - нет питомца
        # 0 - потерял уровень, неизлечим
        # >=1 - имеет уровень, излечим
        if not self['has_pet']:
            return -1
        return int(self['pet']['pet_level'] or 0)

    @property
    def pet_is_dead(self):
        # Питомец КОНТУЖЕН, когда питомец мертв без восстановления -
        # self['pet']['pet_level'] == ''
        return self['has_pet'] and self['pet']['pet_is_dead']

    @property
    def dir_back(self):
        # 'ft' is forward, 'tt' is backward
        return self.hero.dir == 'tt'

    @property
    def field_msg(self):
        return self.get('news_from_field', {}).get('msg')

    @property
    def q2t_after(self):
        q2t = self.hero.get('q2t')
        if q2t:
            parsed = q2t.strip('()').split(':')
            if len(parsed) == 2:
                return (int(parsed[0]) * 60 * 60) + (int(parsed[1]) * 60)
            else:
                assert len(parsed) == 1
                return int(parsed[0]) * 60

    @property
    def wrd_cnt(self):
        if 'wrd' in self.hero:
            return wrd_cnt(self.hero.wrd)
