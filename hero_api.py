import os
import re
import urllib.parse
from collections import OrderedDict
import json
from datetime import timedelta

import js2py
from requests import Session

from hero_utils import maybe_attr_dict


def _get_js_context():
    js_path = os.path.join(os.path.dirname(__file__), 'hero_api.js')

    def _encode_uri(string):
        # https://stackoverflow.com/a/6618858/450103
        return urllib.parse.quote(string.to_python().encode('utf8'),
                                  safe='~@#$&()*!+=:;,.?/\'')
    context = js2py.EvalJs({'encodeURI': _encode_uri})
    context.execute(open(js_path).read())
    return context


class HeroApi:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._js_context = _get_js_context()
        self.session = Session()

        cookies = self.get_state('cookies', True)
        if cookies:
            self.logger.debug('Cookies loaded')
            self.session.cookies = cookies
        else:
            self.login()

    def login(self):
        data = {'username': self.username, 'password': self.password,
                'save_login': True, 'commit': 'Войти!'}
        resp = self.session.post('https://godville.net/login/login', data=data,
                                 **self.params.request)
        assert 'Неправильное имя или пароль.' not in resp.text, 'Auth failed'
        self.logger.info('Logged in as %s', self.username)
        self.set_state('cookies', self.session.cookies, True)
        return resp

    def _send(self, pk, payload=None, subpath=None, check_status='success'):
        url = 'https://godville.net/fbh/feed'
        if subpath:
            url += '/' + subpath

        data = OrderedDict(a=self._js_context.pk(pk))
        if payload:
            payload = json.dumps(payload, separators=(',', ':'))
            data['b'] = self._js_context.qk(payload)

        result = self.session.post(url, data=data, **self.params.request).json()
        assert (not check_status or result['status'] == check_status), result
        return maybe_attr_dict(result)

    def get_info(self):
        url = 'https://godville.net/fbh/feed?a=GjZLI9oQGPkBZMqMMMP3KYBRVcqmu&cnt=-1'
        RETRY_SLEEP = 10  # retry after timeout (seconds)
        RELOGIN_RETRY = 60  # relogin every (RETRY_SLEEP * RELOGIN_RETRY) seconds

        logged_in = None
        retry = 0
        while True:
            retry += 1
            try:
                resp = self.session.get(url, **self.params.request)
                if not resp.status_code == 200:
                    self.sleep(RETRY_SLEEP, reason='Info status code error {}: {} {}'.format(
                        retry, resp.status_code, resp.text))
                    continue
                rv = resp.json()
            except json.JSONDecodeError:
                if logged_in is None or ((retry - logged_in) >= RELOGIN_RETRY):
                    # We got json decode error if session is expired
                    # Just try to relogin
                    self.login()
                    logged_in = retry
                else:
                    self.sleep(RETRY_SLEEP, reason='Info decode error {}: {}'.format(
                        retry, resp.text))
            else:
                if rv.get('expired') or ('hero' in rv and rv['hero'].get('expired')):
                    if logged_in is None or ((retry - logged_in) >= RELOGIN_RETRY):
                        self.login()
                        logged_in = retry
                    else:
                        self.sleep(RETRY_SLEEP, reason='Searching hero {}: {}'.format(retry, rv))
                else:
                    if 'ctime' not in rv:
                        if 'hero' in rv and 'bdirs' in rv and 'r_map' in rv:
                            # this is experiment battle royale, ctime is absent for some reason...
                            return rv
                        self.sleep(RETRY_SLEEP, reason='Unknown json response {}: {}'.format(
                            retry, rv))
                    else:
                        return rv

    def get_info2(self):
        # Looks like the same as get_info, just another endpoint
        return self._send('50hsxFtl89G6dWcalMAU', subpath='vc2')

    def set_motto(self, motto):
        return self._send('WzCGcLhzgkHj35wake2c', dict(motto=motto))

    def get_pantheon_positions(self):
        return self._send('fjQtRFHERdTEvAbY3p2m')

    def get_friends(self):
        return self._send('3hOqa3hA2Gjh7tEWSD5T')

    def get_trained_heroes(self):
        return self._send('W8gW0YrAgDYuBCicRE0v')

    def get_chat(self, god_name):
        return self._send('KztXWDej9YxdBu0LCMUf', dict(name=god_name))

    def send_chat(self, god_name, message):
        return self._send('GJyYqhQtKRYCWFp1UwM5', dict(to=god_name, msg=message))

    def accept_train(self, god_name):
        return self._send('z0uVDOyRiCLKIBhDnnVw', dict(chfr='1', chfr_by=god_name))

    def get_map(self):
        return self._send('VWjdHHidaEM0GdCXtYum')

    def voice(self, phrase):
        rv = self._send('5JgMUahE1BYdtf7quoWz',
                        dict(action='god_phrase', god_phrase=phrase))
        self.logger.info('sent voice: %s', phrase)
        return rv

    def encourage(self):
        return self._send('5JgMUahE1BYdtf7quoWz', dict(action='encourage', s=0))

    def punish(self):
        return self._send('5JgMUahE1BYdtf7quoWz', dict(action='punish', s=0))

    def miracle(self):
        return self._send('5JgMUahE1BYdtf7quoWz', dict(action='third_action', s=0))

    def resurrect(self):
        return self._send('5JgMUahE1BYdtf7quoWz', dict(action='resurrect', s=0))

    def acc_restore(self):
        return self._send('5JgMUahE1BYdtf7quoWz', dict(action='acc_restore'))

    def acc_accumulate(self):
        raise NotImplementedError()

    def inventory_activate(self, name):
        return self._send('agQHqM4rCoT0CaDvq44I', dict(id=name, s=0))

    def check_dungeon_wood(self):
        """Returns timedelta before wood available, or True if available already"""
        rv = self._send('yYKfxTjrO3fuODiRwOOJ')
        self.logger.debug('check_dungeon_wood: %s', rv['msg'])
        if 'Дерево гофер в сокровищнице гарантировано.' in rv['msg']:
            return True
        match = re.match(r'При отправке в ближайшие (\d+)ч (\d+)мин', rv['msg'])
        assert match, 'Unknown check dungeon wood response: {}'.format(rv['msg'])
        return timedelta(hours=int(match.group(1)), minutes=int(match.group(2)))

    def send_to_dungeon(self):
        self._send('3JaEcn7VbOq7sIs8koKn', {'type': 'to_dungeon'}, check_status='proceed')
        return self._send('5JgMUahE1BYdtf7quoWz', dict(action='to_dungeon', s=0))

    def send_to_royale(self):
        self._send('3JaEcn7VbOq7sIs8koKn', {'type': 'to_royale'}, check_status='prompt')
        return self._send('5JgMUahE1BYdtf7quoWz', dict(action='to_royale', s=0))

    def check_arena(self):
        """Returns (ZPG/PVP (arena type), needs confirmation) tuple"""

        data = self._send('3JaEcn7VbOq7sIs8koKn', {'type': 'to_arena'}, check_status=False)
        if data['status'] == 'proceed':
            return 'PVP', False
        if data['status'] == 'deny':
            self.logger.info('arena denied: %s', data.get('desc', data))
            return None, None
        if data['status'] == 'prompt':
            if data.get('cid') == 'zc':
                return 'ZPG', True
            if data.get('cid') == 'ac':
                return 'PVP', True
        raise AssertionError('Unknown response: {}'.format(data))

    def send_to_pvp_arena(self):
        arena_type, confirm = self.check_arena()
        if arena_type == 'PVP':
            if confirm:
                # Only near ZPG arena time prompt for PVP appears
                # NOTE: confirmation payload for PVP not tested, but appears to be like ZPG
                payload = dict(action='to_arena', cid='ac', confirm='1')
            else:
                payload = dict(action='to_arena', s=0)
            return self._send('5JgMUahE1BYdtf7quoWz', payload)

    def send_to_zpg_arena(self):
        arena_type, confirm = self.check_arena()
        if arena_type == 'ZPG':
            return self._send('5JgMUahE1BYdtf7quoWz',
                              dict(action='to_arena', cid='zc', confirm='1'))

    def send_to_sail(self):
        self._send('3JaEcn7VbOq7sIs8koKn', {'type': 'to_sail'}, check_status='proceed')
        return self._send('5JgMUahE1BYdtf7quoWz', dict(action='to_sail', s=0))

    def send_to_train(self, god_name):
        return self._send('5JgMUahE1BYdtf7quoWz', dict(action='chf', opp=god_name))

    def diary_voice_vote(self, vote_id, is_liked):
        return self._send('lvBuKwP7tQetJZNXXQx7', dict(vote_id=vote_id, vote=is_liked))

    def get_laboratory(self):
        return self._send('NFH56LppugOjUtZ1cEyS')

    def remove_from_laboratory(self, part_type):
        # part_type one of skin/horn/ear/eye..
        return self._send('RpChZyuC92rQQ4dXAWUx', dict(t=part_type))

    def bingo_inventory_check(self):
        rv = self.session.post('https://godville.net/news/bgn_show_inventory',
                               **self.params.request).json()
        assert rv['status'] == 'success', rv
        return maybe_attr_dict(rv)

    def bingo_inventory_use(self):
        rv = self.session.post('https://godville.net/news/bgn_use_inventory',
                               **self.params.request).json()
        assert rv['status'] == 'success', rv
        return maybe_attr_dict(rv)

    def gp_cap_check(self):
        """Returns tuple with current godpower, timestamp, already_used for gp_cap_use"""
        res = self.session.post('https://godville.net/news/gp_cap',
                                **self.params.request).json()
        gp = (res[1] - res[2]) * res[3]
        self.logger.debug('GP CAP check: %.2f %s', gp, res)
        # res[4] == 'Сегодня уже разряжался.'
        return gp, res[2], bool(res[4])

    def gp_cap_use(self, timestamp):
        self.logger.debug('GP CAP use: %s', timestamp)
        return self.session.post('https://godville.net/news/gp_cap_use',
                                 data={'s': timestamp}, **self.params.request).json()

    def cross_check(self, data, raise_error=False):
        # data should be dict like:
        # {
        #   # coords (x, y): char
        #   (2, 0): 'В'
        #   (3, 0): 'Р'
        #   (4, 0): 'А'
        # }
        data = {'c[%d][%d]' % (k[0], k[1]): v.upper() for k, v in data.items()}
        rv = self.session.post(
                'https://godville.net/news/check_crossword',
                data=data, **self.params.request).json()
        if raise_error:
            assert 'Кроссворд отгадан' in rv['status'], rv['status']
        return rv

    def collect_coupon(self, raise_error=False):
        rv = self.session.post(
                'https://godville.net/news/collect_coupon',
                **self.params.request).json()
        assert rv['status'] == 'success', rv
        return maybe_attr_dict(rv)
