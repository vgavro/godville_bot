#!/usr/bin/fish

set START (get -p 'START (iso date of first building day, 2017-08-13 for example)')
set WOOD (get -p 'WOOD (current wood count, 796 for 79.6%%)')
set END (get -p 'END (iso date of wood count, leave empty for today)' -d 'today')

echo "WOOD/DAY:"
qalc "$WOOD / days($START, $END)"

echo "PASSED DAYS:"
qalc "days($START, $END)"

echo "TOTAL DAYS:"
qalc "1000 / ($WOOD / days($START, $END))"

echo "ETA:"
qalc "addDays($START, round(1000 / ($WOOD / days($START, $END))))"
